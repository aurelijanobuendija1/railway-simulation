package models;

import interfaces.Renderable;
import ui_components.*;
import javafx.scene.layout.GridPane;
import resources.Colors;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Position implements Renderable, Runnable, Serializable {
    private final int x;
    private final int y;
    private transient boolean locked = false;
    private transient static GridPane GRID_PANE;
    private transient Cell cell;
    private transient CountDownLatch latchToWaitForJavaFx;

    public static void setGridPane(GridPane gridPane) {
        GRID_PANE = gridPane;
    }


    public Position(int x, int y) {
        this.x = x;
        this.y = y;

        this.cell = new ColorizedCell(Colors.EMPTY_CELL);
    }

    public Position(int x, int y, Cell cell) {
        this(x, y);
        this.cell = cell;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public synchronized Cell getCell() {
        return cell;
    }

    public synchronized void setCell(Cell cell) {
        this.cell = cell;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public void setLatchToWaitForJavaFx(CountDownLatch latchToWaitForJavaFx) {
        this.latchToWaitForJavaFx = latchToWaitForJavaFx;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;
        Position position = (Position) o;
        return getX() == position.getX() && getY() == position.getY();
    }

    @Override
    public void Render() {
        synchronized (GRID_PANE) {
            try {
                GRID_PANE.add(this.cell, this.y, this.x);
            } catch (Exception exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            } finally {
                if (null != this.latchToWaitForJavaFx)
                    latchToWaitForJavaFx.countDown();
            }
        }
    }

    @Override
    public void run() {
        this.Render();
    }

    @Override
    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }

    public boolean isBeforeThan(Position position, CopyOnWriteArrayList<Position> trajectory) {
        int myIndex = -1;
        int otherIndex = -1;

        int iter = 0;
        for (Position p : trajectory) {
            if (p.equals(this)) {
                myIndex = iter;
            }
            if (p.equals(position)) {
                otherIndex = iter;
            }
            iter++;
        }


        return otherIndex != -1 && myIndex <= otherIndex;
    }
}
