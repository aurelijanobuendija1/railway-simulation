package models.dynamical;

import ui_components.Cell;
import ui_components.ImageCell;
import javafx.application.Platform;
import models.Position;
import models.Simulation;
import models.statical.RoadLane;

import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Vehicle implements Runnable {
    protected Position currentPosition;
    protected Position nextPosition;
    protected RoadLane roadLane;

    protected String name;
    protected String brand;
    protected String model;
    protected int year;
    protected int speed;

    static int COUNTER = 0;

    public Vehicle(RoadLane roadLane, int speed, String brand, String model, int year) {
        this.roadLane = roadLane;
        this.speed = speed;
        this.brand = brand;
        this.model = model;
        this.year = year;

        this.name = this.roadLane.getName() + "_Car-" + (COUNTER++);
    }

    public abstract Cell getNewCell();

    public void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    public void await(CountDownLatch latch) {
        try {
            latch.await();
        } catch (InterruptedException exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    public void waitIfRailwayCrossing() {
        if (this.roadLane.getRoad().isPositionRailwayCrossing(this.nextPosition)) {
            while (this.roadLane.getRoad().getRailwayCrossing().isTrainComing()) {
                this.sleep(100);
            }
        }
    }

    public void enter() {
        this.nextPosition = this.roadLane.getNextPosition(null);
        while (this.nextPosition.getCell() instanceof ImageCell || this.nextPosition.isLocked()) {
            this.sleep(200);
        }
        this.nextPosition.setLocked(true);

        CountDownLatch latch = new CountDownLatch(1);

        this.nextPosition.setCell(this.getNewCell());
        this.nextPosition.setLatchToWaitForJavaFx(latch);
        Platform.runLater(this.nextPosition);
        this.await(latch);
        this.currentPosition = this.nextPosition;
        this.nextPosition = this.roadLane.getNextPosition(this.currentPosition);
        this.sleep(this.speed);
        this.currentPosition.setLocked(false);
    }

    public void move() {
        CountDownLatch latch = new CountDownLatch(2);

        Position previousPosition = this.currentPosition;
        previousPosition.setLatchToWaitForJavaFx(latch);
        previousPosition.setCell(this.roadLane.getNewCell(previousPosition));
        this.currentPosition = this.nextPosition;

        this.nextPosition.setLatchToWaitForJavaFx(latch);
        this.currentPosition.setCell(this.getNewCell());

        Platform.runLater(previousPosition);
        Platform.runLater(this.currentPosition);

        this.await(latch);

        this.nextPosition = this.roadLane.getNextPosition(this.currentPosition);

        this.currentPosition.setLocked(false);
        previousPosition.setLocked(false);
    }

    public void exit() {
        while (this.currentPosition.isLocked()) {
        }
        this.currentPosition.setLocked(true);

        CountDownLatch latch = new CountDownLatch(1);

        this.currentPosition.setCell(this.roadLane.getNewCell(this.currentPosition));
        this.currentPosition.setLatchToWaitForJavaFx(latch);
        Platform.runLater(this.currentPosition);

        this.await(latch);

        this.currentPosition.setLocked(false);
    }

    @Override
    public void run() {
        this.enter();
        while (null != this.nextPosition) {
            if (this.nextPosition.getCell() instanceof ImageCell || this.nextPosition.isLocked() || this.currentPosition.isLocked()) {
                continue;
            }

            this.currentPosition.setLocked(true);
            this.nextPosition.setLocked(true);

            this.waitIfRailwayCrossing();

            this.move();

            this.sleep(this.speed);
        }

        this.exit();
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "roadLane=" + roadLane +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", speed=" + speed +
                '}';
    }

    public RoadLane getRoadLane() {
        return roadLane;
    }
}
