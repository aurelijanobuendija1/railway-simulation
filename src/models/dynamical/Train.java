package models.dynamical;

import interfaces.Compositionable;
import javafx.application.Platform;
import models.Position;
import models.Simulation;
import models.statical.Railway;
import models.statical.TrainStation;
import util.PathList;
import util.Serializer;
import util.TrainEvent;
import util.TrainHistory;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Train implements Runnable {
    private CopyOnWriteArrayList<Compositionable> composition;
    private LinkedHashMap<Position, Compositionable> positionOnRailway;
    private TrainStation startStation;
    private TrainStation endStation;
    private final String name;
    private Railway currentRailway;

    private Position firstPosition;
    private Position currentPosition;
    private Position tailPosition;
    private final int speed;

    private final TrainHistory trainHistory;

    public Train(String name, int speed, TrainStation startStation, TrainStation endStation) {
        this.name = name;
        this.speed = speed;
        this.startStation = startStation;
        this.endStation = endStation;
        this.trainHistory = new TrainHistory();
    }

    public TrainStation getStartStation() {
        return startStation;
    }

    public void setStartStation(TrainStation startStation) {
        this.startStation = startStation;
    }

    public TrainStation getEndStation() {
        return endStation;
    }

    public void setEndStation(TrainStation endStation) {
        this.endStation = endStation;
    }

    public void setCurrentRailway(Railway railway) {
        this.currentRailway = railway;
    }

    public Position getLastPosition() {
        return this.positionOnRailway.size() > 0 ? (Position) this.positionOnRailway.keySet().toArray()[this.positionOnRailway.size() - 1] : null;
    }

    public void setTailPosition() {
        for (Position position : this.positionOnRailway.keySet()) {
            this.tailPosition = position;
        }
    }

    public synchronized void moveAll() {
        Compositionable lastRendered = null;
        LinkedHashMap<Position, Compositionable> old = new LinkedHashMap<>(this.positionOnRailway);
        for (Position position : this.positionOnRailway.keySet()) {
            Compositionable compositionable = this.positionOnRailway.get(position);
            Position nextPosition = this.currentRailway.getNextPosition(position);
            old.remove(position);
            if (null == nextPosition) {
                compositionable.setPosition(null);
            } else {
                old.put(nextPosition, compositionable);
                compositionable.setPosition(nextPosition);
                nextPosition.setCell(compositionable.getNewCell());
            }
            lastRendered = compositionable;
        }
        int lastRenderedIndex = this.composition.indexOf(lastRendered);
        if (lastRenderedIndex < this.composition.size() - 1) {
            Compositionable nextToRender = this.composition.get(lastRenderedIndex + 1);
            Position nextToRenderPosition = this.currentRailway.getNextPosition(null);
            nextToRenderPosition.setCell(nextToRender.getNewCell());
            nextToRender.setPosition(nextToRenderPosition);
            old.put(nextToRenderPosition, nextToRender);
        }
        this.positionOnRailway = old;
    }

    public void await(CountDownLatch latch) {
        try {
            latch.await();
        } catch (InterruptedException exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    public void sleep() {
        try {
            Thread.sleep(100);
        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    public void enterTheRailway() {
        this.positionOnRailway = new LinkedHashMap<>();

        CountDownLatch latch = new CountDownLatch(1);

        this.tailPosition = this.firstPosition;
        this.currentPosition = this.firstPosition;

        this.firstPosition.setCell(this.composition.get(0).getNewCell());
        this.firstPosition.setLatchToWaitForJavaFx(latch);
        this.composition.get(0).setPosition(this.firstPosition);
        this.positionOnRailway.put(this.firstPosition, this.composition.get(0));
        Platform.runLater(this.firstPosition);

        this.await(latch);
        TrainStation trainStation = this.currentRailway.areCurrentRidesInverse() ? this.currentRailway.getTrainStationTo() : this.currentRailway.getTrainStationFrom();
        this.trainHistory.pushEvent(TrainEvent.leaveStation(trainStation));

        this.trainHistory.pushEvent(TrainEvent.moveToPosition(this.firstPosition));
    }

    public void erase() {
        int threadCount = this.positionOnRailway.size();
        CountDownLatch latch = new CountDownLatch(threadCount);
        for (Position position : this.positionOnRailway.keySet()) {
            position.setLatchToWaitForJavaFx(latch);
            position.setCell(this.currentRailway.getNewCell(position));
            Platform.runLater(position);
        }

        this.await(latch);
        if (this.positionOnRailway.size() == this.composition.size() && this.tailPosition.equals(this.firstPosition)) {
            this.firstPosition.setLocked(false);
        }
        if (!this.tailPosition.equals(this.firstPosition)) {
            this.tailPosition.setLocked(false);
        }
    }

    public void draw() {
        int threadCount = this.positionOnRailway.size();
        CountDownLatch latch = new CountDownLatch(threadCount);
        this.setCurrentPosition();

        for (Position position : this.positionOnRailway.keySet()) {
            position.setLatchToWaitForJavaFx(latch);
            position.setCell(this.positionOnRailway.get(position).getNewCell());
            Platform.runLater(position);
        }

        this.await(latch);
        this.setTailPosition();
        this.tailPosition.setLocked(true);

    }

    public void setCurrentPosition() {
        if (this.positionOnRailway.size() == 0) {
            this.currentPosition = null;
        } else {
            this.currentPosition = this.positionOnRailway.entrySet().iterator().next().getKey();
                this.trainHistory.pushEvent(TrainEvent.moveToPosition(this.currentPosition));
        }
    }

    @Override
    public void run() {
        if (this.currentRailway.getTrainStationFrom().equals(this.startStation) || this.currentRailway.getTrainStationTo().equals(this.startStation)) {
            this.trainHistory.start();
        }
        this.firstPosition = this.currentRailway.getNextPosition(null);

        while (this.firstPosition.isLocked()) {
            this.sleep();
        }
        this.firstPosition.setLocked(true);
        this.enterTheRailway();
        this.sleep();

        while (!this.positionOnRailway.isEmpty()) {
            if (null != this.currentRailway.getNextPosition(this.currentPosition) && this.currentRailway.getNextPosition(this.currentPosition).isLocked()) {
                sleep();
                continue;
            }
            this.erase();
            this.moveAll();
            this.draw();

            try {
                Thread.sleep(this.speed);
            } catch (Exception exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            }
        }
        this.tailPosition.setLocked(false);

        this.currentRailway.arrivedOnTheNextStation(this);
    }

    public Position getTailPosition() {
        return tailPosition;
    }

    public void setComposition(CopyOnWriteArrayList<Compositionable> composition) {
        this.composition = composition;
    }

    @Override
    public String toString() {
        return this.name + " (" +
                     this.startStation.getName() + " - " +
                     this.endStation.getName() + ")";
    }

    public TrainHistory getTrainHistory() {
        return this.trainHistory;
    }

    public String getName() {
        return name;
    }

    public void serialize() {
        File f = new File(PathList.TRAIN_MOVEMENTS_DIRECTORY);
        if (!f.exists()) {
            f.mkdir();
        }
        Serializer.save(this.trainHistory, PathList.TRAIN_MOVEMENTS_DIRECTORY, this + ".ser");
    }
}
