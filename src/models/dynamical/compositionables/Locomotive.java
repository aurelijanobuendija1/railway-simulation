package models.dynamical.compositionables;

import ui_components.Cell;
import ui_components.ImageCell;
import interfaces.Compositionable;
import models.Position;
import models.dynamical.Train;
import resources.Images;

public class Locomotive implements Compositionable {
    public enum LocomotiveType {PASSENGER, FREIGHT, UNIVERSAL, MANEUVER}

    public enum DriveType {STEAM, DIESEL, ELECTRIC}

    private Position position;
    private Train train;

    private LocomotiveType locomotiveType;
    private DriveType driveType;
    private double power;

    private String label;

    public Locomotive(Train train, String label, LocomotiveType locomotiveType, DriveType driveType, double power) {
        this.train = train;
        this.label = label;
        this.locomotiveType = locomotiveType;
        this.driveType = driveType;
        this.power = power;
    }

    public Cell getNewCell() {
        return new ImageCell(Images.LOCOMOTIVE_IMAGES.get(this.driveType));
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocomotiveType getLocomotiveType() {
        return locomotiveType;
    }

    public DriveType getDriveType() {
        return driveType;
    }
}
