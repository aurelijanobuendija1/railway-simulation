package models.dynamical.compositionables;

import ui_components.Cell;
import interfaces.Compositionable;
import javafx.scene.image.Image;
import models.Position;
import models.dynamical.Train;

public abstract class Wagon implements Compositionable {
    private static Image IMAGE = new Image("resources/icons/wagon_1.jpeg");
    private Position position;
    private Train train;

    private String label;
    private double length;

    public Wagon(Train train, String label, double length) {
        this.train = train;
        this.label = label;
        this.length = length;
    }

    public abstract Cell getNewCell();

    public Position getPosition() {
        return position;
    }

    public Train getTrain() {
        return train;
    }

    @Override
    public void setPosition(Position position) {
        this.position = position;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
