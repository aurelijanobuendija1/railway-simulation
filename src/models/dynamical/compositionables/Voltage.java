package models.dynamical.compositionables;

import ui_components.Cell;
import ui_components.ColorizedCell;
import interfaces.Compositionable;
import models.Position;
import models.dynamical.Train;
import resources.Colors;

public class Voltage implements Compositionable {
    private Position position;
    private Train train;

    public Voltage(Train train) {
        this.train = train;
    }

    @Override
    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public Position getPosition() {
        return this.position;
    }

    @Override
    public Cell getNewCell() {
        return new ColorizedCell(Colors.VOLTAGE_COLOR);
    }

    @Override
    public String getLabel() {
        return null;
    }
}
