package models.dynamical.compositionables.wagons.passenger_wagons;

import ui_components.Cell;
import ui_components.ImageCell;
import models.dynamical.Train;
import models.dynamical.compositionables.wagons.PassengerWagon;
import resources.Images;

public class SeatAndBedWagon extends PassengerWagon {
    private int numberOfSeats;
    private int numberOfBeds;

    public SeatAndBedWagon(Train train, String label, double length, int numberOfSeats, int numberOfBeds) {
        super(train, label, length);
        this.numberOfBeds = numberOfBeds;
        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public Cell getNewCell() {
        return new ImageCell(Images.SEAT_AND_BED_WAGON);
    }

}
