package models.dynamical.compositionables.wagons.passenger_wagons;

import ui_components.Cell;
import ui_components.ImageCell;
import models.dynamical.Train;
import models.dynamical.compositionables.wagons.PassengerWagon;
import resources.Images;

public class RestaurantWagon extends PassengerWagon {
    private String description;

    public RestaurantWagon(Train train, String label, double length, String description) {
        super(train, label, length);
        this.description = description;
    }

    @Override
    public Cell getNewCell() {
        return new ImageCell(Images.RESTAURANT_WAGON);
    }
}
