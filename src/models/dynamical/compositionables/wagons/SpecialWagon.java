package models.dynamical.compositionables.wagons;

import ui_components.Cell;
import ui_components.ImageCell;
import models.dynamical.Train;
import models.dynamical.compositionables.Wagon;
import resources.Images;

public class SpecialWagon extends Wagon {
    public SpecialWagon(Train train, String label, double length) {
        super(train, label, length);
    }

    @Override
    public Cell getNewCell() {
        return new ImageCell(Images.SPECIAL_WAGON);
    }

}
