package models.statical;

import ui_components.Cell;
import ui_components.ColorizedCell;
import models.Position;
import models.dynamical.Train;
import resources.Colors;
import util.Trajectories;

import java.util.concurrent.CopyOnWriteArrayList;

public class RailwayCrossing {
    private final String name;
    private final CopyOnWriteArrayList<Position> trajectory;
    private Railway railway;

    public RailwayCrossing(String name) throws Exception {
        if (!Trajectories.RAILWAY_CROSSINGS.containsKey(name)) {
            throw new Exception("Railway crossing does not exists");
        }
        this.name = name;
        this.trajectory = new CopyOnWriteArrayList<>(Trajectories.RAILWAY_CROSSINGS.getOrDefault(this.name, new Position[]{}));

        this.trajectory.forEach((position) -> position.setCell(getNewCell()));
    }

    public Cell getNewCell() {
        return new ColorizedCell(Colors.RAILWAY_CROSSING_COLOR);
    }

    public void Render() {
        this.trajectory.forEach(Position::Render);
    }

    public void setRailway(Railway railway) {
        this.railway = railway;
    }

    public String getName() {
        return this.name;
    }

    public CopyOnWriteArrayList<Position> getTrajectory() {
        return this.trajectory;
    }

    public synchronized boolean isTrainComing() {
        if (this.railway.getCurrentTrains().isEmpty()) {
            return false;
        }
        for (Train train : this.railway.getCurrentTrains()) {
            for (Position p : this.trajectory) {
                if (null != train.getTailPosition() && train.getTailPosition().isBeforeThan(p, this.railway.getTrajectory())) {
                    return true;
                }
            }

        }
        return false;
    }
}

