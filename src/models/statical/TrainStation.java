package models.statical;

import ui_components.ColorizedCell;
import interfaces.Renderable;
import models.Position;
import models.Simulation;
import models.dynamical.Train;
import resources.Colors;
import util.TrainEvent;
import util.Trajectories;
import util.Dijkstra;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;

public class TrainStation implements Renderable, Serializable {

    private final String name;
    private final transient CopyOnWriteArrayList<Position> coordinates;
    private final transient HashMap<Railway, Queue<Train>> railwayQueueHashMap;


    public TrainStation(String name) throws Exception {
        if (!Trajectories.TRAIN_STATIONS.containsKey(name)) {
            throw new Exception("Train station does not exists");
        }
        this.name = name;
        this.coordinates = new CopyOnWriteArrayList<>(Trajectories.TRAIN_STATIONS.getOrDefault(this.name, new Position[]{}));
        this.coordinates.forEach((coordinate) -> coordinate.setCell(new ColorizedCell(Colors.TRAIN_STATION_COLOR)));
        this.railwayQueueHashMap = new HashMap<>();
    }

    public void redirectTrain(Train train) {
        train.getTrainHistory().pushEvent(TrainEvent.arriveToStation(this));
        TrainStation endStation = train.getEndStation();
        Railway nextRailway;

        Dijkstra dijkstra = new Dijkstra(Simulation.RAILWAY_GRAPH);
        dijkstra.execute(this);
        LinkedList<TrainStation> path = dijkstra.getPath(endStation);

        if (null != path) {
            nextRailway = this.railwayQueueHashMap.keySet()
                    .stream()
                    .filter((railway) -> railway.getName().contains(path.get(1).getName()))
                    .findFirst().get();
            nextRailway.putTrainToQueue(train, this);
        } else {
            train.getTrainHistory().finish();
            train.serialize();
        }


    }

    public String getName() {
        return name;
    }

    @Override
    public void Render() {
        this.coordinates.forEach(Position::Render);
    }

    public HashMap<Railway, Queue<Train>> getRailwayQueueHashMap() {
        return railwayQueueHashMap;
    }
}
