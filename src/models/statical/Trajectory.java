package models.statical;

import ui_components.Cell;
import interfaces.Renderable;
import models.Position;
import models.Simulation;
import util.Trajectories;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Trajectory implements Renderable {
    protected String name;
    protected CopyOnWriteArrayList<Position> coordinates;

    public Trajectory(String name, Cell cell) throws Exception {
        if (!Trajectories.ROAD_LANES.containsKey(name)) {
            throw new Exception("Road lane does not exists");
        }
        this.name = name;
        this.coordinates = new CopyOnWriteArrayList<>(Trajectories.ROAD_LANES.getOrDefault(this.name, new Position[]{}));
        this.coordinates.forEach((coordinate) -> {
            try {
                coordinate.setCell((Cell) cell.clone());
            } catch (Exception exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            }
        });
    }


    @Override
    public void Render() {
        this.coordinates.forEach(Position::Render);
    }
}
