package models;

import exceptions.UnprocessableConfigurationException;
import interfaces.Renderable;
import models.statical.*;
import util.PathList;
import util.Trajectories;
import util.RailwayGraph;
import watchers.ConfigurationWatcher;
import watchers.TrainLinesWatcher;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.*;

public class Simulation implements Renderable {
    public static boolean RUNNING = true;
    public static Handler LOG_HANDLER;
    public static Logger LOGGER = Logger.getLogger(Simulation.class.getName());

    {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            Date date = new Date();
            Path fileName = Paths.get(PathList.LOGS_DIRECTORY + "/" + formatter.format(date) + "/simulation.log");
            if (!Files.exists(fileName.getParent())) {
                Files.createDirectory(fileName.getParent());
            }
            LOG_HANDLER = new FileHandler(fileName.toAbsolutePath().toString());
            LOGGER.addHandler(LOG_HANDLER);
        } catch (IOException exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    private final CopyOnWriteArrayList<Road> roads = new CopyOnWriteArrayList<>();
    private final CopyOnWriteArrayList<TrainStation> trainStations = new CopyOnWriteArrayList<>();
    private final CopyOnWriteArrayList<Railway> railways = new CopyOnWriteArrayList<>();
    private final CopyOnWriteArrayList<RailwayCrossing> railwayCrossings = new CopyOnWriteArrayList<>();
    private ConfigurationWatcher configurationWatcher;
    private TrainLinesWatcher trainLinesWatcher;

    public static RailwayGraph RAILWAY_GRAPH;

    public Simulation() {
        this.initRoads();
        this.initTrainStations();
        this.initRailways();
        this.initRailwayCrossings();
        this.roads.forEach(road -> road.setRailwayCrossing(
                railwayCrossings
                        .stream()
                        .filter(railwayCrossing -> railwayCrossing.getName().equals(String.valueOf(road.getName().charAt(5))))
                        .findFirst()
                        .get()
        ));

        RAILWAY_GRAPH = new RailwayGraph(trainStations, railways);
        try {
            this.configurationWatcher = new ConfigurationWatcher(this);
            this.configurationWatcher.handle(Paths.get(PathList.CONFIGURATION_FILE));
            this.trainLinesWatcher = new TrainLinesWatcher(this);
            this.trainLinesWatcher.readInitialTrainLines(Paths.get(PathList.INITIAL_TRAIN_LINES_DIRECTORY));
        } catch (IOException | UnprocessableConfigurationException exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    public void initRoads() {
        for (String road_name : Trajectories.ROADS) {
            this.roads.add(new Road(road_name));
        }
    }

    public void initTrainStations() {
        for (String train_station_name : Trajectories.TRAIN_STATIONS.keySet()) {
            try {
                this.trainStations.add(new TrainStation(train_station_name));
            } catch (Exception exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            }
        }
    }

    public void initRailwayCrossings() {
        Map<Integer, String> deps = Map.of(
                1, "AB",
                2, "BC",
                3, "CE"
        );
        for (String name : Trajectories.RAILWAY_CROSSINGS.keySet()) {
            try {
                RailwayCrossing newCrossing = new RailwayCrossing(name);
                newCrossing.setRailway(railways.stream().filter((railway -> railway.getName().equals(deps.get(Integer.valueOf(name))))).findFirst().get());
                this.railwayCrossings.add(newCrossing);
                railways.stream().filter(railway -> railway.getName().equals(deps.get(Integer.valueOf(name)))).findFirst().get()
                        .setRailwayCrossing(newCrossing);


            } catch (Exception exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            }
        }
    }

    public void initRailways() {
        for (String railway_name : Trajectories.RAILWAYS.keySet()) {
            try {
                TrainStation trainStationFrom = trainStations.stream().filter((trainStation) -> trainStation.getName().equals(String.valueOf(railway_name.charAt(0)))).findFirst().get();
                TrainStation trainStationTo = trainStations.stream().filter((trainStation) -> trainStation.getName().equals(String.valueOf(railway_name.charAt(1)))).findFirst().get();

                Railway newRailway = new Railway(railway_name, trainStationFrom, trainStationTo);
                trainStationFrom.getRailwayQueueHashMap().put(newRailway, new LinkedList<>());
                trainStationTo.getRailwayQueueHashMap().put(newRailway, new LinkedList<>());
                this.railways.add(newRailway);
            } catch (Exception exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            }
        }
    }


    @Override
    public void Render() {
        this.roads.forEach(Road::Render);
        this.trainStations.forEach(TrainStation::Render);
        this.railways.forEach(Railway::Render);
        this.railwayCrossings.forEach(RailwayCrossing::Render);
    }

    public void startSimulation() {
        new Thread(this.configurationWatcher).start();
        new Thread(this.trainLinesWatcher).start();

        this.railways.forEach(railway -> new Thread(railway).start());
        this.roads.forEach((road) -> new Thread(road).start());
    }

    public CopyOnWriteArrayList<Road> getRoads() {
        return roads;
    }

    public CopyOnWriteArrayList<TrainStation> getTrainStations() {
        return trainStations;
    }

}
