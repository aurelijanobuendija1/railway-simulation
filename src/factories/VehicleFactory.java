package factories;

import factories.vehicles.CarFactory;
import factories.vehicles.TruckFactory;
import models.dynamical.Vehicle;
import models.statical.Road;

import java.util.List;
import java.util.Random;

public class VehicleFactory {
    private enum Factories {CAR_FACTORY, TRUCK_FACTORY}

    private static final List<Factories> vehicle_factories = List.of(Factories.CAR_FACTORY, Factories.TRUCK_FACTORY);

    public static Vehicle generate(Road road) {
        switch (vehicle_factories.get(new Random().nextInt(vehicle_factories.size()))) {
            case CAR_FACTORY:
                return CarFactory.generate(road);
            case TRUCK_FACTORY:
                return TruckFactory.generate(road);
        }
        return CarFactory.generate(road);
    }
}
