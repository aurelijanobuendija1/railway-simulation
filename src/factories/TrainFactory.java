package factories;

import exceptions.UnprocessableConfigurationException;
import factories.compositionables.LocomotiveFactory;
import factories.compositionables.wagons.CargoWagonFactory;
import factories.compositionables.wagons.SpecialWagonFactory;
import factories.compositionables.wagons.passenger_wagons.RestaurantWagonFactory;
import factories.compositionables.wagons.passenger_wagons.SeatAndBedWagonFactory;
import interfaces.Compositionable;
import javafx.util.Pair;
import models.Simulation;
import models.dynamical.Train;
import models.dynamical.compositionables.Locomotive;
import models.dynamical.compositionables.Voltage;
import models.dynamical.compositionables.Wagon;
import models.dynamical.compositionables.wagons.CargoWagon;
import models.dynamical.compositionables.wagons.PassengerWagon;
import models.dynamical.compositionables.wagons.SpecialWagon;
import models.statical.TrainStation;
import util.Trajectories;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class TrainFactory {
    private static int MAX_COMPOSITION_SIZE = 17;

   static  {
        List<Integer> railwaySizes = Trajectories.RAILWAYS.values().stream().map(railway -> railway.length).collect(Collectors.toList());
        railwaySizes.stream().mapToInt(v -> v).min().ifPresent(TrainFactory::setMaxCompositionSize);
    }

    private static void setMaxCompositionSize(int size) {
       MAX_COMPOSITION_SIZE = size;
    }

    public static Train generate(Simulation simulation, List<String> config) throws UnprocessableConfigurationException {
        try {
            Train train = initTrain(simulation, config.get(0));
            config.remove(0);
            CopyOnWriteArrayList<Compositionable> compositionables = new CopyOnWriteArrayList<>();
            for (String configLine : config) {
                String[] parameters = configLine.split(";");
                Compositionable compositionable;
                switch (parameters[0]) {
                    case "Locomotive":
                        compositionable = LocomotiveFactory.generate(train, parameters);
                        break;
                    case "SeatAndBedWagon":
                        compositionable = SeatAndBedWagonFactory.generate(train, parameters);
                        break;
                    case "RestaurantWagon":
                        compositionable = RestaurantWagonFactory.generate(train, parameters);
                        break;
                    case "CargoWagon":
                        compositionable = CargoWagonFactory.generate(train, parameters);
                        break;
                    case "SpecialWagon":
                        compositionable = SpecialWagonFactory.generate(train, parameters);
                        break;
                    default:
                        throw new UnprocessableConfigurationException("Unsupported composition element");
                }
                compositionables.add(compositionable);
            }
            if (!validateComposition(compositionables)) {
                throw new UnprocessableConfigurationException("Bad combination of composition elements");
            }

            if(compositionables.size() > MAX_COMPOSITION_SIZE) {
                throw  new UnprocessableConfigurationException("Size of composition greater than " + MAX_COMPOSITION_SIZE);
            }
            setVoltage(compositionables, train);
            train.setComposition(compositionables);

            return train;
        } catch (UnprocessableConfigurationException exception) {
            throw exception;
        } catch (Exception exception) {
            throw new UnprocessableConfigurationException("Bad train configuration");
        }
    }

    private static void setVoltage(CopyOnWriteArrayList<Compositionable> compositionables, Train train) {
        if (((Locomotive) compositionables.get(0)).getDriveType() == Locomotive.DriveType.ELECTRIC) {
            compositionables.add(0, new Voltage(train));
            compositionables.add(compositionables.size(), new Voltage(train));
        }
    }

    private static Train initTrain(Simulation simulation, String configLine) throws UnprocessableConfigurationException {
        String[] parameters = configLine.split(";");
        if (!"Train".equals(parameters[0])) {
            throw new UnprocessableConfigurationException("First line of train configuration must contain train information");
        }
        try {
            TrainStation startStation = simulation.getTrainStations().stream().filter(trainStation -> trainStation.getName().equals(parameters[3])).findFirst().get();
            TrainStation endStation = simulation.getTrainStations().stream().filter(trainStation -> trainStation.getName().equals(parameters[4])).findFirst().get();
            int speed = Integer.parseInt(parameters[2]);
            Train train = new Train(
                    parameters[1], //label
                    Math.min(speed, 500), //speed
                    startStation,
                    endStation
            );
            startStation.redirectTrain(train);
            return train;
        } catch (Exception exception) {
            throw new UnprocessableConfigurationException("Error in train related line of configuration");
        }
    }

    private static List<List<Locomotive.LocomotiveType>> LOCOMOTIVE_MATCHES = List.of(
            List.of(
                    Locomotive.LocomotiveType.PASSENGER
            ),
            List.of(
                    Locomotive.LocomotiveType.FREIGHT
            ),
            List.of(
                    Locomotive.LocomotiveType.MANEUVER
            ),
            List.of(
                    Locomotive.LocomotiveType.UNIVERSAL
            ),
            List.of(
                    Locomotive.LocomotiveType.PASSENGER,
                    Locomotive.LocomotiveType.UNIVERSAL
            ),
            List.of(
                    Locomotive.LocomotiveType.FREIGHT,
                    Locomotive.LocomotiveType.UNIVERSAL
            ),
            List.of(
                    Locomotive.LocomotiveType.MANEUVER,
                    Locomotive.LocomotiveType.UNIVERSAL
            )
    );


    private static boolean validateComposition(CopyOnWriteArrayList<Compositionable> compositionables) {
        if (!(compositionables.get(0) instanceof Locomotive)) return false;

        List<Compositionable> locmotives = compositionables.stream().filter(compositionable -> compositionable instanceof Locomotive).collect(Collectors.toList());
        List<Compositionable> wagons = compositionables.stream().filter(compositionable -> compositionable instanceof Wagon).collect(Collectors.toList());

        Locomotive.LocomotiveType locomotiveType = determineLocomotiveType(locmotives);
        if (null == locomotiveType) {
            return false;
        }
        if(wagons.size() == 0) {
            return true;
        }

        return validateWagons(wagons, locomotiveType);
    }

    private static Locomotive.LocomotiveType determineLocomotiveType(List<Compositionable> locomotives) {
        Locomotive.LocomotiveType type;

        List<Locomotive.LocomotiveType> locomotiveTypes = locomotives.stream()
                .map(locomotive -> ((Locomotive) locomotive).getLocomotiveType())
                .collect(Collectors.toList());

        for (List<Locomotive.LocomotiveType> allowed : LOCOMOTIVE_MATCHES) {
            if (locomotiveTypes.stream().allMatch(locomotiveType -> allowed.contains(locomotiveType))) {
                type = allowed.get(0);
                return type;
            }
        }

        return null;
    }

    private static List<Pair<Locomotive.LocomotiveType, Locomotive.LocomotiveType>> LOCOMOTIVE_COMPOSITION_MATCHES =
            List.of(
                    new Pair<>(Locomotive.LocomotiveType.PASSENGER, Locomotive.LocomotiveType.PASSENGER),
                    new Pair<>(Locomotive.LocomotiveType.FREIGHT, Locomotive.LocomotiveType.FREIGHT),
                    new Pair<>(Locomotive.LocomotiveType.UNIVERSAL, Locomotive.LocomotiveType.PASSENGER),
                    new Pair<>(Locomotive.LocomotiveType.UNIVERSAL, Locomotive.LocomotiveType.FREIGHT),
                    new Pair<>(Locomotive.LocomotiveType.MANEUVER, Locomotive.LocomotiveType.MANEUVER)
            );

    private static boolean validateWagons(List<Compositionable> wagons, Locomotive.LocomotiveType locomotiveType) {
        Locomotive.LocomotiveType compositionType = null;

        if (wagons.stream().allMatch(wagon -> wagon instanceof PassengerWagon)) {
            compositionType = Locomotive.LocomotiveType.PASSENGER;
        } else if (wagons.stream().allMatch(wagon -> wagon instanceof CargoWagon)) {
            compositionType = Locomotive.LocomotiveType.FREIGHT;
        } else if (wagons.stream().allMatch(wagon -> wagon instanceof SpecialWagon)) {
            compositionType = Locomotive.LocomotiveType.MANEUVER;
        }

        return (LOCOMOTIVE_COMPOSITION_MATCHES.contains(new Pair<>(locomotiveType, compositionType)));
    }
}
