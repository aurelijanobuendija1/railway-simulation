package factories.vehicles;

import models.dynamical.vehicles.Car;
import models.statical.Road;
import models.statical.RoadLane;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class CarFactory {
    private static final Map<String, List<String>> BRANDS = Map.of(
            "Zastava", List.of(
                    "101",
                    "Yugo Koral",
                    "Yugo Florida"
            ),
            "Volkswagen", List.of(
                    "Golf 1",
                    "Golf 2",
                    "Golf 3",
                    "Polo"
            ),
            "Opel", List.of(
                    "Astra",
                    "Zafira"
            )
    );

    public static Car generate(Road road) {
        RoadLane roadLane = road.getRoadLanes().get(new Random().nextInt(road.getRoadLanes().size()));
        String brand = (String) BRANDS.keySet().toArray()[(int) (new Random().nextInt(BRANDS.size()))];
        List<String> models = BRANDS.get(brand);
        String model = models.get(new Random().nextInt(models.size()));
        int year = new Random().nextInt(50) + 1960;
        int speed = road.getSpeedLimit() + new Random().nextInt(500 - road.getSpeedLimit());
        int numberOfDoors = new Random().nextInt(5) + 2;
        return new Car(
                roadLane,
                speed,
                brand,
                model,
                year,
                numberOfDoors
        );
    }
}
