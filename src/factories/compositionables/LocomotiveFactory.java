package factories.compositionables;

import exceptions.UnprocessableConfigurationException;
import models.dynamical.Train;
import models.dynamical.compositionables.Locomotive;

public class LocomotiveFactory {
    public static Locomotive generate(Train train, String[] parameters) throws UnprocessableConfigurationException {
        try {
            return new Locomotive(
                    train,
                    parameters[1], //label
                    Locomotive.LocomotiveType.valueOf(parameters[2]), //locomotiveType
                    Locomotive.DriveType.valueOf(parameters[3]), //driveType
                    Double.parseDouble(parameters[4]) //power
            );
        } catch (Exception exception) {
            throw new UnprocessableConfigurationException("Bad locomotive configuration");
        }
    }

}
