package factories.compositionables.wagons.passenger_wagons;

import exceptions.UnprocessableConfigurationException;
import models.dynamical.Train;
import models.dynamical.compositionables.wagons.passenger_wagons.SeatAndBedWagon;

public class SeatAndBedWagonFactory {
    public static SeatAndBedWagon generate(Train train, String[] parameters) throws UnprocessableConfigurationException {
        try {
            return new SeatAndBedWagon(
                    train,
                    parameters[1], //label
                    Double.parseDouble(parameters[2]), //length
                    Integer.parseInt(parameters[3]), //numberOfSeats
                    Integer.parseInt(parameters[4]) //numberOfBeds
            );
        } catch (Exception exception) {
            throw new UnprocessableConfigurationException("Bad seat and bad wagon configuration");
        }
    }
}