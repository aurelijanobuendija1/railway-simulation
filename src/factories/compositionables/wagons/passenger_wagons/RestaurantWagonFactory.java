package factories.compositionables.wagons.passenger_wagons;

import exceptions.UnprocessableConfigurationException;
import models.dynamical.Train;
import models.dynamical.compositionables.wagons.passenger_wagons.RestaurantWagon;

public class RestaurantWagonFactory {
    public static RestaurantWagon generate(Train train, String[] parameters) throws UnprocessableConfigurationException {
        try {
            return new RestaurantWagon(
                    train,
                    parameters[1], //label
                    Double.parseDouble(parameters[2]), //length
                    parameters[3] //description
            );
        } catch (Exception exception) {
            throw new UnprocessableConfigurationException("Bad restaurant wagon configuration");
        }
    }
}
