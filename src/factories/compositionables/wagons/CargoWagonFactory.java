package factories.compositionables.wagons;

import exceptions.UnprocessableConfigurationException;
import models.dynamical.Train;
import models.dynamical.compositionables.wagons.CargoWagon;

public class CargoWagonFactory {
    public static CargoWagon generate(Train train, String[] parameters) throws UnprocessableConfigurationException {
        try {
            return new CargoWagon(
                    train,
                    parameters[1], //label
                    Double.parseDouble(parameters[2]), //length
                    Double.parseDouble(parameters[3]) //capacity
            );
        } catch (Exception exception) {
            throw new UnprocessableConfigurationException("Bad cargo wagon configuration");
        }
    }
}
