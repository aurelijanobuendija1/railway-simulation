package factories.compositionables.wagons;

import exceptions.UnprocessableConfigurationException;
import models.dynamical.Train;
import models.dynamical.compositionables.wagons.SpecialWagon;

public class SpecialWagonFactory {
    public static SpecialWagon generate(Train train, String[] parameters) throws UnprocessableConfigurationException {
        try {
            return new SpecialWagon(
                    train,
                    parameters[1], //label
                    Double.parseDouble(parameters[2]) //length
            );
        } catch (Exception exception) {
            throw new UnprocessableConfigurationException("Bad special wagon configuration");
        }
    }
}
