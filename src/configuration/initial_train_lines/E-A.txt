#Train; name; speed; startStationName; endStationName;
#Locomotive; label; locomotiveType: {PASSENGER, FREIGHT, UNIVERSAL, MANEUVER}; driveType: {STEAM, DIESEL, ELECTRIC}; power
#SeatAndBedWagon; label; length; numberOfSeats; numberOfBeds
#RestaurantWagon; label; length; description
#CargoWagon; label; length; capacity
#SpecialWagon; label; length;

Train;Voz4;200;E;A;
Locomotive;Lokomotiva1;UNIVERSAL;ELECTRIC;250;
SeatAndBedWagon;Vagon1;100;100;50;
RestaurantWagon;Vagon2;140;SUR Orao;
RestaurantWagon;Vagon3;140;SUR Zoki;
#SpecialWagon;label;13;
