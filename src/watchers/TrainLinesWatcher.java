package watchers;

import exceptions.UnprocessableConfigurationException;
import factories.TrainFactory;
import models.Simulation;
import util.PathList;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class TrainLinesWatcher extends AbstractWatcher implements Runnable {

    private final Path success_directory = Paths.get(PathList.TRAIN_CONFIGURATION_DIRECTORY + "/successful");
    private final Path failed_directory = Paths.get(PathList.TRAIN_CONFIGURATION_DIRECTORY + "/failed");

    public TrainLinesWatcher(Simulation simulation) throws IOException {
        super(simulation, PathList.TRAIN_CONFIGURATION_DIRECTORY, StandardWatchEventKinds.ENTRY_CREATE);
    }


    public void handle(Path fileName) throws IOException, UnprocessableConfigurationException {
        try {
            List<String> content = Files.readAllLines(this.directory.resolve(fileName)).
                    stream().filter(line -> !line.startsWith("#") && !line.isEmpty()).collect(Collectors.toList());
            TrainFactory.generate(this.simulation, content);
        } catch (UnprocessableConfigurationException exception) {
            throw new UnprocessableConfigurationException(fileName + ": " + exception.getMessage());
        }
    }

    public void readInitialTrainLines(Path path) throws IOException, UnprocessableConfigurationException {
        try {
            File folder = path.toAbsolutePath().toFile();
            File[] listOfLines = folder.listFiles();
            if (null != listOfLines) {
                for (File file : listOfLines) {
                    this.handle(file.toPath());
                }
            }
        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    public void moveFile(Path directoryPath, Path filename) {
        try {
            Files.move(this.directory.resolve(filename), Paths.get(directoryPath + "/" + filename), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    @Override
    public void onSuccess(Path filename) {
        this.moveFile(this.success_directory, filename);
    }

    @Override
    public void onFail(Path filename) {
        this.moveFile(this.failed_directory, filename);
    }
}