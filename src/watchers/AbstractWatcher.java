package watchers;

import exceptions.UnprocessableConfigurationException;
import models.Simulation;

import java.io.IOException;
import java.nio.file.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractWatcher implements Runnable {
    protected WatchService watchService;
    protected Path directory;
    protected WatchKey key;
    protected Simulation simulation;
    protected WatchEvent.Kind eventKind;

    public AbstractWatcher(Simulation simulation, String path, WatchEvent.Kind eventKind) throws IOException {
        this.watchService = FileSystems.getDefault().newWatchService();
        this.directory = Paths.get(path).toAbsolutePath();
        this.simulation = simulation;
        this.eventKind = eventKind;
        directory.register(this.watchService, this.eventKind);
    }

    @Override
    public void run() {
        try {
            while (Simulation.RUNNING) {
                try {
                    this.key = this.watchService.take();
                } catch (InterruptedException exception) {
                    Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
                    return;
                }
                Thread.sleep(10);
                for (WatchEvent<?> event : key.pollEvents()) {
                    WatchEvent.Kind<?> kind = event.kind();
                    WatchEvent<Path> ev = (WatchEvent<Path>) event;
                    Path fileName = (Path) ev.context();
                    if (kind.equals(this.eventKind)) {
                        try {
                            this.handle(fileName);
                            this.onSuccess(fileName);
                        } catch (Exception exception) {
                            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
                            onFail(fileName);
                        }
                    }
                }
                boolean valid = key.reset();
                if (!valid) {
                    break;
                }
            }
        } catch (InterruptedException exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    public abstract void handle(Path fileName) throws IOException, UnprocessableConfigurationException;

    public void onSuccess(Path filename) {}
    public void onFail(Path filename) {}
}
