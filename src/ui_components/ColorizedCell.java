package ui_components;

public class ColorizedCell extends Cell {
    private String color;

    public ColorizedCell(String color) {
        this.color = color;

        String css = "-fx-background-color: " + this.color;

        this.setStyle(css);
    }
}
