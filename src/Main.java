import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import models.Simulation;

import java.util.Objects;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("resources/views/simulation.fxml")));
        primaryStage.setTitle("Railway");
        primaryStage.setScene(new Scene(root, 900, 900));
        primaryStage.setOnCloseRequest(we -> {
            Simulation.RUNNING = false;
            System.exit(0);
        });
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
