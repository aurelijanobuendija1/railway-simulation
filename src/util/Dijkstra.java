package util;

import models.statical.Railway;
import models.statical.TrainStation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Dijkstra {

    private final List<TrainStation> nodes;
    private final List<Railway> edges;
    private Set<TrainStation> settledNodes;
    private Set<TrainStation> unSettledNodes;
    private Map<TrainStation, TrainStation> predecessors;
    private Map<TrainStation, Integer> distance;

    public Dijkstra(RailwayGraph graph) {
        // create a copy of the array so that we can operate on this array
        this.nodes = new ArrayList<>(graph.getVertexes());
        this.edges = new ArrayList<>(graph.getEdges());
    }

    public void execute(TrainStation source) {
        settledNodes = new HashSet<>();
        unSettledNodes = new HashSet<>();
        distance = new HashMap<>();
        predecessors = new HashMap<>();
        distance.put(source, 0);
        unSettledNodes.add(source);
        while (unSettledNodes.size() > 0) {
            TrainStation node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            findMinimalDistances(node);
        }
    }

    private void findMinimalDistances(TrainStation node) {
        List<TrainStation> adjacentNodes = getNeighbors(node);
        for (TrainStation target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node)
                    + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node)
                        + getDistance(node, target));
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }

    }

    private int getDistance(TrainStation node, TrainStation target) {
        for (Railway edge : edges) {
            if (edge.getTrainStationFrom().equals(node)
                    && edge.getTrainStationTo().equals(target)) {
                return edge.getTrajectory().size();
            }
        }
        throw new RuntimeException("Should not happen");
    }

    private List<TrainStation> getNeighbors(TrainStation node) {
        List<TrainStation> neighbors = new ArrayList<TrainStation>();
        for (Railway edge : edges) {
            if (edge.getTrainStationFrom().equals(node)
                    && !isSettled(edge.getTrainStationTo())) {
                neighbors.add(edge.getTrainStationTo());
            }
        }
        return neighbors;
    }

    private TrainStation getMinimum(Set<TrainStation> vertexes) {
        TrainStation minimum = null;
        for (TrainStation vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
                    minimum = vertex;
                }
            }
        }
        return minimum;
    }

    private boolean isSettled(TrainStation vertex) {
        return settledNodes.contains(vertex);
    }

    private int getShortestDistance(TrainStation destination) {
        Integer d = distance.get(destination);
        if (d == null) {
            return Integer.MAX_VALUE;
        } else {
            return d;
        }
    }

    /*
     * This method returns the path from the source to the selected target and
     * NULL if no path exists
     */
    public LinkedList<TrainStation> getPath(TrainStation target) {
        LinkedList<TrainStation> path = new LinkedList<TrainStation>();
        TrainStation step = target;
        // check if a path exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }

}