package util;

import models.Position;
import models.statical.TrainStation;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TrainEvent implements Serializable {
    public enum EventType {LEAVE_STATION, ARRIVE_TO_STATION, WAITING_ON_STATION, MOVED_TO_POSITION}

    private final EventType eventType;
    private TrainStation trainStation;
    private final Long timestamp;
    private Long waitingTime;
    private Position position;

    private TrainEvent(EventType eventType, TrainStation trainStation) {
        this.eventType = eventType;
        this.trainStation = trainStation;
        this.timestamp = System.currentTimeMillis();
    }

    private TrainEvent(EventType eventType, Position position) {
        this.eventType = eventType;
        this.position = position;
        this.timestamp = System.currentTimeMillis();
    }

    public static TrainEvent leaveStation(TrainStation trainStation) {
        return new TrainEvent(EventType.LEAVE_STATION, trainStation);
    }

    public static TrainEvent arriveToStation(TrainStation trainStation) {
        return new TrainEvent(EventType.ARRIVE_TO_STATION, trainStation);
    }

    public static TrainEvent startWaiting(TrainStation trainStation) {
        return new TrainEvent(EventType.WAITING_ON_STATION, trainStation);
    }

    public void finishWaiting(TrainEvent finishEvent) {
        if (EventType.WAITING_ON_STATION.equals(this.eventType) && null != this.timestamp) {

            this.waitingTime = finishEvent.getTimestamp() - this.timestamp;
        }
    }

    public static TrainEvent moveToPosition(Position position) {
        return new TrainEvent(EventType.MOVED_TO_POSITION, position);
    }

    @Override
    public String toString() {
        switch (eventType) {
            case LEAVE_STATION:
                return "Leaved station " +
                        this.trainStation.getName() +
                        " at " +
                        prettifyTimestamp(this.timestamp, true);
            case ARRIVE_TO_STATION:
                return "Arrived to station " +
                        this.trainStation.getName() +
                        " at " +
                        prettifyTimestamp(this.timestamp, true);
            case WAITING_ON_STATION:
                return "Waiting on station " +
                        this.trainStation.getName() +
                        " for " +
                        prettifyTimestamp(this.waitingTime, false);
            case MOVED_TO_POSITION:
                return "Moved to position " +
                        this.position;

        }
        return "TrainEvent{" +
                "eventType=" + eventType +
                ", trainStation=" + trainStation +
                ", timestamp=" + timestamp +
                ", waitingTime=" + waitingTime +
                ", position=" + position +
                '}';
    }

    public static String prettifyTimestamp(Long timestamp, boolean withHours) {
        Date date = new Date(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat(withHours ? "HH:mm:ss.SSS" : "mm:ss.SSS");
        return sdf.format(date);
    }

    public EventType getEventType() {
        return this.eventType;
    }

    public TrainStation getTrainStation() {
        return this.trainStation;
    }

    public Long getTimestamp() {
        return this.timestamp;
    }

    public Position getPosition() {
        return position;
    }
}
