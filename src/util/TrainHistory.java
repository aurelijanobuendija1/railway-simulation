package util;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class TrainHistory implements Serializable {
    private final CopyOnWriteArrayList<TrainEvent> events;
    private Long startedAt;
    private Long finishedAt;

    private TrainEvent waitingEvent;

    public TrainHistory() {
        this.events = new CopyOnWriteArrayList<>();
    }

    public void start() {
        this.startedAt = System.currentTimeMillis();
    }

    public void finish() {
        this.finishedAt = System.currentTimeMillis();
    }

    public void pushEvent(TrainEvent trainEvent) {
        if (TrainEvent.EventType.ARRIVE_TO_STATION.equals(trainEvent.getEventType())) {
            this.waitingEvent = TrainEvent.startWaiting(trainEvent.getTrainStation());
        } else if (TrainEvent.EventType.LEAVE_STATION.equals(trainEvent.getEventType()) && (null != this.waitingEvent)) {
            this.waitingEvent.finishWaiting(trainEvent);
            this.events.add(this.waitingEvent);
            this.waitingEvent = null;
        } else if (TrainEvent.EventType.MOVED_TO_POSITION.equals(trainEvent.getEventType()) &&
                this.events.stream().filter(event -> TrainEvent.EventType.MOVED_TO_POSITION.equals(event.getEventType()))
                        .anyMatch(event -> trainEvent.getPosition().equals(event.getPosition()))) {
            return;
        }
        this.events.add(trainEvent);
    }

    @Override
    public String toString() {
        return "Started at: " +
                TrainEvent.prettifyTimestamp(this.startedAt, true) + "\n" +
                "Finished at: " +
                TrainEvent.prettifyTimestamp(this.finishedAt, true) + "\n" +
                "Duration: " + TrainEvent.prettifyTimestamp(this.finishedAt - this.startedAt, false) + "\n\n" +
                "History: \n\n" +
                this.events.stream().map(Object::toString).collect(Collectors.joining(",\n"));
    }
}
