package util;

import models.Simulation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Serializer {
    public static <T> T readObject(String fullPath) {
        ObjectInputStream ois = null;
        T object = null;
        try {
            ois = new ObjectInputStream(new FileInputStream(fullPath));
            object = (T) ois.readObject();
            ois.close();

        } catch (IOException | ClassNotFoundException exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        } finally {
            try {
                ois.close();
            } catch (IOException exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            }
        }
        return object;
    }

    public static <T> boolean save(T object, String path, String filename) {
        ObjectOutputStream oos = null;
        boolean result = false;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(path + File.separator + filename));
            oos.writeObject(object);
            result = true;
        } catch (IOException exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        } finally {
            try {
                oos.close();
            } catch (IOException exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            }
        }
        return result;
    }
}