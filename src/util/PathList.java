package util;

import models.Simulation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PathList {
    public static final String CONFIGURATION_DIRECTORY = "src/configuration";
    public static final String CONFIGURATION_FILE = "config.txt";

    public static final String INITIAL_TRAIN_LINES_DIRECTORY = "src/configuration/initial_train_lines";
    public static String TRAIN_CONFIGURATION_DIRECTORY = "src/configuration/train_lines";
    public static String TRAIN_MOVEMENTS_DIRECTORY = "src/train_movements";

    public static final String LOGS_DIRECTORY = "src/logs";

    static {
        try {
            List<String> config_lines = Files.readAllLines(Paths.get(CONFIGURATION_DIRECTORY + "/" + CONFIGURATION_FILE).toAbsolutePath());
            config_lines.stream()
                    .filter(line -> line.startsWith("TRAIN_MOVEMENTS_DIRECTORY"))
                    .findFirst().ifPresent(line -> setTrainMovementsDirectory(line.split("=")[1]));
            config_lines.stream()
                    .filter(line -> line.startsWith("TRAIN_LINES_DIRECTORY"))
                    .findFirst().ifPresent(line -> setTrainConfigurationDirectory(line.split("=")[1]));
        } catch (IOException exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }

    }

    private static void setTrainConfigurationDirectory(String path) {
        TRAIN_CONFIGURATION_DIRECTORY = path;
    }

    private static void setTrainMovementsDirectory(String path) {
        TRAIN_MOVEMENTS_DIRECTORY = path;
    }
}
