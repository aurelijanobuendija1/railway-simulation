package util;

import models.Simulation;
import models.statical.Railway;
import models.statical.TrainStation;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RailwayGraph {
    private final List<TrainStation> vertexes;
    private final List<Railway> edges;

    public RailwayGraph(List<TrainStation> vertexes, List<Railway> edges) {
        this.vertexes = vertexes;
        this.edges = new ArrayList<>(edges);
        edges.forEach(edge -> {
            try {
                Railway inverseRailway = new Railway(edge.getName(), edge.getTrainStationTo(), edge.getTrainStationFrom());
                inverseRailway.setName(new StringBuilder(inverseRailway.getName()).reverse().toString());
                this.edges.add(inverseRailway);
            } catch (Exception exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            }
        });
    }

    public List<TrainStation> getVertexes() {
        return vertexes;
    }

    public List<Railway> getEdges() {
        return edges;
    }
}
