package util;

import models.Position;

import java.util.List;
import java.util.Map;

public class Trajectories {
    public static final List<String> ROADS = List.of("ROAD_1", "ROAD_2", "ROAD_3");
    public static final Map<String, Position[]> ROAD_LANES = Map.of(
            "ROAD_1_RIGHT", new Position[]{
                    new Position(29, 8),
                    new Position(28, 8),
                    new Position(27, 8),
                    new Position(26, 8),
                    new Position(25, 8),
                    new Position(24, 8),
                    new Position(23, 8),
                    new Position(22, 8),
                    new Position(21, 8),
                    new Position(20, 8),

                    new Position(20, 7),
                    new Position(20, 6),
                    new Position(20, 5),
                    new Position(20, 4),
                    new Position(20, 3),
                    new Position(20, 2),
                    new Position(20, 1),
                    new Position(20, 0),
            },
            "ROAD_1_LEFT", new Position[]{
                    new Position(29, 7),
                    new Position(28, 7),
                    new Position(27, 7),
                    new Position(26, 7),
                    new Position(25, 7),
                    new Position(24, 7),
                    new Position(23, 7),
                    new Position(22, 7),
                    new Position(21, 7),

                    new Position(21, 6),
                    new Position(21, 5),
                    new Position(21, 4),
                    new Position(21, 3),
                    new Position(21, 2),
                    new Position(21, 1),
                    new Position(21, 0)
            },
            "ROAD_2_RIGHT", new Position[]{
                    new Position(29, 14),
                    new Position(28, 14),
                    new Position(27, 14),
                    new Position(26, 14),
                    new Position(25, 14),
                    new Position(24, 14),
                    new Position(23, 14),
                    new Position(22, 14),
                    new Position(21, 14),
                    new Position(20, 14),
                    new Position(19, 14),

                    new Position(18, 14),
                    new Position(17, 14),
                    new Position(16, 14),
                    new Position(15, 14),
                    new Position(14, 14),
                    new Position(13, 14),
                    new Position(12, 14),
                    new Position(11, 14),
                    new Position(10, 14),
                    new Position(9, 14),
                    new Position(8, 14),

                    new Position(7, 14),
                    new Position(6, 14),
                    new Position(5, 14),
                    new Position(4, 14),
                    new Position(3, 14),
                    new Position(2, 14),
                    new Position(1, 14),
                    new Position(0, 14),

            },
            "ROAD_2_LEFT", new Position[]{

                    new Position(29, 13),
                    new Position(28, 13),
                    new Position(27, 13),
                    new Position(26, 13),
                    new Position(25, 13),
                    new Position(24, 13),
                    new Position(23, 13),
                    new Position(22, 13),
                    new Position(21, 13),
                    new Position(20, 13),
                    new Position(19, 13),

                    new Position(18, 13),
                    new Position(17, 13),
                    new Position(16, 13),
                    new Position(15, 13),
                    new Position(14, 13),
                    new Position(13, 13),
                    new Position(12, 13),
                    new Position(11, 13),
                    new Position(10, 13),
                    new Position(9, 13),
                    new Position(8, 13),

                    new Position(7, 13),
                    new Position(6, 13),
                    new Position(5, 13),
                    new Position(4, 13),
                    new Position(3, 13),
                    new Position(2, 13),
                    new Position(1, 13),
                    new Position(0, 13)
            },
            "ROAD_3_RIGHT", new Position[]{
                    new Position(29, 22),
                    new Position(28, 22),
                    new Position(27, 22),
                    new Position(26, 22),
                    new Position(25, 22),
                    new Position(24, 22),
                    new Position(23, 22),
                    new Position(22, 22),
                    new Position(21, 22),

                    new Position(21, 23),
                    new Position(21, 24),
                    new Position(21, 25),
                    new Position(21, 26),
                    new Position(21, 27),
                    new Position(21, 28),
                    new Position(21, 29),
            },
            "ROAD_3_LEFT", new Position[]{
                    new Position(29, 21),
                    new Position(28, 21),
                    new Position(27, 21),
                    new Position(26, 21),
                    new Position(25, 21),
                    new Position(24, 21),
                    new Position(23, 21),
                    new Position(22, 21),
                    new Position(21, 21),
                    new Position(20, 21),

                    new Position(20, 22),
                    new Position(20, 23),
                    new Position(20, 24),
                    new Position(20, 25),
                    new Position(20, 26),
                    new Position(20, 27),
                    new Position(20, 28),
                    new Position(20, 29),
            });

    public static final Map<String, Position[]> TRAIN_STATIONS = Map.of(
            "A", new Position[]{
                    new Position(27, 1),
                    new Position(27, 2),
                    new Position(28, 1),
                    new Position(28, 2),
            },
            "B", new Position[]{
                    new Position(5, 6),
                    new Position(5, 7),
                    new Position(6, 6),
                    new Position(6, 7),
            },
            "C", new Position[]{
                    new Position(12, 19),
                    new Position(12, 20),
                    new Position(13, 19),
                    new Position(13, 20),
            },
            "D", new Position[]{
                    new Position(1, 26),
                    new Position(1, 27),
                    new Position(2, 26),
                    new Position(2, 27),
            },
            "E", new Position[]{
                    new Position(25, 25),
                    new Position(25, 26),
                    new Position(26, 25),
                    new Position(26, 26),
            });

    public static final Map<String, Position[]> RAILWAYS = Map.of(
            "AB", new Position[]{
                    new Position(26, 2),
                    new Position(25, 2),
                    new Position(24, 2),
                    new Position(23, 2),
                    new Position(22, 2),
                    new Position(21, 2),
                    new Position(20, 2),
                    new Position(19, 2),
                    new Position(18, 2),
                    new Position(17, 2),

                    new Position(17, 3),
                    new Position(17, 4),
                    new Position(17, 5),

                    new Position(16, 5),
                    new Position(15, 5),
                    new Position(14, 5),
                    new Position(13, 5),
                    new Position(12, 5),
                    new Position(11, 5),
                    new Position(10, 5),
                    new Position(9, 5),
                    new Position(8, 5),
                    new Position(7, 5),
                    new Position(6, 5),
            },
            "BC", new Position[]{
                    new Position(6, 8),
                    new Position(6, 9),
                    new Position(6, 10),
                    new Position(6, 11),
                    new Position(6, 12),
                    new Position(6, 13),
                    new Position(6, 14),
                    new Position(6, 15),
                    new Position(6, 16),
                    new Position(6, 17),
                    new Position(6, 18),
                    new Position(6, 19),

                    new Position(7, 19),
                    new Position(8, 19),
                    new Position(9, 19),
                    new Position(10, 19),
                    new Position(11, 19),
            },
            "CD", new Position[]{
                    new Position(12, 21),
                    new Position(12, 22),
                    new Position(12, 23),
                    new Position(12, 24),
                    new Position(12, 25),
                    new Position(12, 26),

                    new Position(11, 26),
                    new Position(10, 26),
                    new Position(9, 26),

                    new Position(9, 27),
                    new Position(9, 28),

                    new Position(8, 28),
                    new Position(7, 28),
                    new Position(6, 28),
                    new Position(5, 28),

                    new Position(5, 27),
                    new Position(5, 26),
                    new Position(5, 25),
                    new Position(5, 24),
                    new Position(5, 23),

                    new Position(4, 23),
                    new Position(3, 23),

                    new Position(3, 22),

                    new Position(2, 22),
                    new Position(1, 22),

                    new Position(1, 23),
                    new Position(1, 24),
                    new Position(1, 25),
            },
            "CE", new Position[]{
                    new Position(14, 20),
                    new Position(15, 20),
                    new Position(16, 20),
                    new Position(17, 20),
                    new Position(18, 20),

                    new Position(18, 21),
                    new Position(18, 22),
                    new Position(18, 23),
                    new Position(18, 24),
                    new Position(18, 25),
                    new Position(18, 26),

                    new Position(19, 26),
                    new Position(20, 26),
                    new Position(21, 26),
                    new Position(22, 26),
                    new Position(23, 26),
                    new Position(24, 26)

            }
    );

    public static final Map<String, Position[]> RAILWAY_CROSSINGS = Map.of(
            "1", new Position[]{
                    new Position(20, 2),
                    new Position(21, 2),
            },
            "2", new Position[]{
                    new Position(6, 13),
                    new Position(6, 14),
            },
            "3", new Position[]{
                    new Position(20, 26),
                    new Position(21, 26),
            }
    );
}