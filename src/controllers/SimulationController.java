package controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import models.Position;
import models.Simulation;

public class SimulationController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btnStart;

    @FXML
    private Button btnDetails;

    @FXML
    private GridPane gridPane;

    @FXML
    private ListView<?> listViewLog;

    private Simulation simulation;

    @FXML
    void btnDetailsClicked(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/views/details.fxml"));

            Scene scene = new Scene(loader.load());
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Train details");
            stage.show();
        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    @FXML
    void btnStartClicked(ActionEvent event) {
        this.gridPane.getChildren().removeAll();
        Position.setGridPane(this.gridPane);
        this.simulation = new Simulation();
        this.simulation.Render();
        try {
            this.simulation.startSimulation();
        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    @FXML
    void initialize() {
        assert btnStart != null : "fx:id=\"btnStart\" was not injected: check your FXML file 'railway_tmp.fxml'.";
        assert btnDetails != null : "fx:id=\"btnDetails\" was not injected: check your FXML file 'railway_tmp.fxml'.";
        assert gridPane != null : "fx:id=\"gridPane\" was not injected: check your FXML file 'railway_tmp.fxml'.";
        assert listViewLog != null : "fx:id=\"listViewLog\" was not injected: check your FXML file 'railway_tmp.fxml'.";

        Position.setGridPane(this.gridPane);
        this.simulation = new Simulation();
        this.simulation.Render();
    }
}
