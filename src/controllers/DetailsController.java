package controllers;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import util.PathList;
import util.Serializer;
import util.TrainHistory;

public class DetailsController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ListView<String> listViewTrains;

    @FXML
    private TextArea textAreaDetails;

    private File file = new File(PathList.TRAIN_MOVEMENTS_DIRECTORY);
    private TrainHistory trainHistory;

    @FXML
    void listClicked(MouseEvent event) {
        this.trainHistory = Serializer.readObject(PathList.TRAIN_MOVEMENTS_DIRECTORY + "/" +
                listViewTrains.getSelectionModel().getSelectedItem() + ".ser");
        this.textAreaDetails.setText(this.trainHistory.toString());
    }

    public void initList() {
        ObservableList<String> items = FXCollections.observableArrayList();
        listViewTrains.setItems(items);
        File[] files = this.file.listFiles();
        if (null != files && files.length > 0) {
            for (var el : files) {
                items.add(el.getName().replace(".ser", ""));
            }
        }
    }

    @FXML
    void initialize() {
        this.initList();
        textAreaDetails.setEditable(false);

        assert listViewTrains != null : "fx:id=\"listViewTrains\" was not injected: check your FXML file 'Untitled'.";
        assert textAreaDetails != null : "fx:id=\"textAreaDetails\" was not injected: check your FXML file 'Untitled'.";
    }

}
