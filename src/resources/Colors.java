package resources;

public class Colors {
    public static final String EMPTY_CELL = "white";
    public static final String ROAD_COLOR = "#abc4ce";
    public static final String RAILWAY_COLOR = "#af9276";
    public static final String TRAIN_STATION_COLOR = "black";
    public static final String RAILWAY_CROSSING_COLOR = "brown";
    public static final String VOLTAGE_COLOR = "#bf9276";
}
